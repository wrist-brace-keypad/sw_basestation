/*
 * app_nrf24l01.h
 *
 *  Created on: May 3, 2017
 */

#ifndef APP_NRF24L01_H_
#define APP_NRF24L01_H_

#include "app_spi.h"

///   ***   Commands   ***
#define NRF24L01_CMD_REG_READ			0x00
#define NRF24L01_CMD_REG_WRITE			0x20
#define NRF24L01_CMD_PAYLOAD_RX_READ	0x61
#define NRF24L01_CMD_PAYLOAD_TX_WRITE	0xA0
#define NRF24L01_CMD_FLUSH_TX			0xE1
#define NRF24L01_CMD_FLUSH_RX			0xE2

///   ***   Register Addresses   ***
//#define NRF24L01_
#define NRF24L01_REG_CONFIG			0x00
#define NRF24L01_REG_EN_AA			0x01
#define NRF24L01_REG_EN_RXADDR		0x02
#define NRF24L01_REG_SETUP_AW		0x03
#define NRF24L01_REG_SETUP_RETR		0x04
#define NRF24L01_REG_RF_CH			0x05
#define NRF24L01_REG_RF_SETUP		0x06
#define NRF24L01_REG_STATUS			0x07
#define NRF24L01_REG_OBSERVE_TX		0x08
#define NRF24L01_REG_RX_ADDR_P0		0x0A
#define NRF24L01_REG_RX_ADDR_P1		0x0B
#define NRF24L01_REG_RX_ADDR_P2		0x0C
#define NRF24L01_REG_RX_ADDR_P3		0x0D
#define NRF24L01_REG_RX_ADDR_P4		0x0E
#define NRF24L01_REG_RX_ADDR_P5		0x0F
#define NRF24L01_REG_TX_ADDR		0x10
#define NRF24L01_REG_RX_PW_P0		0x11
#define NRF24L01_REG_RX_PW_P1		0x12
#define NRF24L01_REG_RX_PW_P2		0x13
#define NRF24L01_REG_RX_PW_P3		0x14
#define NRF24L01_REG_RX_PW_P4		0x15
#define NRF24L01_REG_RX_PW_P5		0x16
#define NRF24L01_REG_FIFO_STATUS	0x17
#define NRF24L01_REG_DYNPD			0x1C
#define NRF24L01_REG_FEATURE		0x1D
#define NRF24L01_REG_UNLOCK			0x30

///   ***   Register Flags   ***
#define NRF24L01_FLAG_CONFIG_PRIM_RX 	0x01
#define NRF24L01_FLAG_CONFIG_PWR_UP		0x02
#define NRF24L01_FLAG_CONFIG_CRC_2B		0x04
#define NRF24L01_FLAG_CONFIG_CRC_EN		0x08
#define NRF24L01_FLAG_FIFO_TX_EMPTY		0x10
#define NRF24L01_FLAG_FIFO_RX_EMPTY		0x01
#define NRF24L01_FLAG_STAT_RX_DR		0x40
#define NRF24L01_FLAG_STAT_TX_DS		0x20
#define NRF24L01_FLAG_STAT_MAX_RT		0x10


///   ***   Library Flags   ***
#define NRF24L01_FLAG_RESUME_RX			1
#define NRF24L01_FLAG_RESUME_TX			0

#define NRF24L01_RUN_MODE_TX			0
#define NRF24L01_RUN_MODE_RX			1

typedef struct {
	GPIO_TypeDef* CE_Port;
	uint16_t CE_Pin;
	SpiInfoStruct Spi_Info;
	uint8_t Adr_Read[5];
	uint8_t Adr_Write[5];
	uint8_t Cur_Run_Mode;

} Nrf24L01_Struct;

///   ***   Public Functions   ***
void App_Nrf24L01_Init(Nrf24L01_Struct *Nrf24L01);
uint8_t App_Nrf24L01_Write_Multi_Packet(Nrf24L01_Struct *Nrf24L01, uint8_t *Bfr, uint8_t Len, uint8_t ResumeInRxMode);
uint8_t App_Nrf24L01_Read_Multi_Packet(Nrf24L01_Struct *Nrf24L01, uint8_t *Bfr, uint8_t *Len);
uint8_t App_Nrf24L01_Write_Packet(Nrf24L01_Struct *Nrf24L01, uint8_t *Bfr, uint8_t Len, uint8_t ResumeInRxMode);
uint8_t App_Nrf24L01_Read_Packets(Nrf24L01_Struct *Nrf24L01, uint8_t *Bfr, uint8_t *Len);


///   ***   Private Functions   ***
void Prv_Nrf24L01_Set_Mode_Rx(Nrf24L01_Struct *Nrf24L01);
void Prv_Nrf24L01_Set_Mode_Tx(Nrf24L01_Struct *Nrf24L01);
void Prv_Nrf24L01_Write_Register(SpiInfoStruct *Spi, uint8_t RegAdr, uint8_t RegData);
uint8_t Prv_Nrf24L01_Read_Register(SpiInfoStruct *Spi, uint8_t RegAdr);
uint8_t Prv_Nrf24L01_Wait_For_Empty_Tx(SpiInfoStruct *Spi);
uint8_t Prv_Nrf24L01_Wait_For_Tx(SpiInfoStruct *Spi);
void Prv_Nrf24L01_Clear_Int_Flags(SpiInfoStruct *Spi);
void Prv_Nrf24L01_Flush_Tx_Fifo(SpiInfoStruct *Spi);
void Prv_Nrf24L01_Flush_Rx_Fifo(SpiInfoStruct *Spi);
uint8_t Prv_Nrf24L01_Get_Status(SpiInfoStruct *Spi);
void Prv_Nrf24L01_Clear_Status_Flag(SpiInfoStruct *Spi, uint8_t Flag);

///   ***   Test Functions
void Tst_Nrf24L01_Spi_RMW(Nrf24L01_Struct *Nrf24L01);

#endif /* APP_NRF24L01_H_ */
