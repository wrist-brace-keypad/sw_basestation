/*
 * app_spi.h
 *
 *  Created on: Apr 20, 2017
 */

#ifndef APP_SPI_H_
#define APP_SPI_H_

//#include <stm32f3xx_hal.h>
#include <stm32f1xx_hal.h>

#define SPI_CSN_KEEP_LOW	0
#define SPI_CSN_ALLOW_HIGH	1

typedef struct {
	GPIO_TypeDef* Ncs_Port;
	uint16_t Ncs_Pin;
	SPI_HandleTypeDef *pHspi;
	DMA_HandleTypeDef *pHdma_Spi_Rx;
	DMA_HandleTypeDef *pHdma_Spi_Tx;
} SpiInfoStruct;

//Transmits and Receives data over SPI using DMA.  Blocks until xfer is complete.
HAL_StatusTypeDef App_Spi_Tx_Rx_DMA_Block(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer, uint8_t *RxBuffer, uint16_t Size);

//Transmits and Receives data over SPI using DMA.  Blocks until xfer is complete.
//if SetCns is set to 1, chip select will go back high at end of xfer.  If set to 0, chip select stays low
HAL_StatusTypeDef App_Spi_Tx_Rx_DMA_Block_CSnCtl(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer, uint8_t *RxBuffer, uint16_t Size, uint8_t SetCns);

//Executes a SPI DMA Tx, then a SPI DMA Rx, while keeping Ncs low between the two actions.  This allows for different sizes of tx and rx buffers
HAL_StatusTypeDef App_Spi_Tx_Rx_Separate_DMA_Block(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer, uint16_t TxSize, uint8_t *RxBuffer, uint16_t RxSize);

//Executes a SPI DMA Tx, then a second DMA Tx, while keeping Ncs low between the two actions.  This allows for use of separate tx buffers
HAL_StatusTypeDef App_Spi_Tx_Tx_Separate_DMA_Block(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer1, uint16_t TxSize1, uint8_t *TxBuffer2, uint16_t TxSize2);

//Executes 3x SPI DMA Tx's, while keeping Ncs low between the two actions.  This allows for use of separate tx buffers,
HAL_StatusTypeDef App_Spi_3Tx_Separate_DMA_Block(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer1, uint16_t TxSize1, uint8_t *TxBuffer2, uint16_t TxSize2, uint8_t *TxBuffer3, uint16_t TxSize3);

#endif /* APP_SPI_H_ */
