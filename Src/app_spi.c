/*
 * app_spi.c
 *
 *  Created on: Apr 20, 2017
 */

#include "app_spi.h"
//#include <stm32f3xx_hal.h>
#include <stm32f1xx_hal.h>
#include <stm32f1xx_hal_spi.h>

HAL_StatusTypeDef App_Spi_Tx_Rx_DMA_Block(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer, uint8_t *RxBuffer, uint16_t Size) {
	HAL_StatusTypeDef errorcode;

	SpiInfo->Ncs_Port->BRR = SpiInfo->Ncs_Pin;												//Set CS low
	//HAL_SPIEx_FlushRxFifo(SpiInfo->pHspi);				//Flush Rx buffer
	if (TxBuffer == NULL) {		//tx buffer is null, so only receive data
		errorcode = HAL_SPI_Receive_DMA(SpiInfo->pHspi, RxBuffer, Size);		//Start Xfer
		while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Rx) != HAL_DMA_STATE_READY);					//Wait for DMA to complete
	} else if (RxBuffer == NULL) {	//rx buffer is null, so only transmit data
		errorcode = HAL_SPI_Transmit_DMA(SpiInfo->pHspi, TxBuffer, Size);		//Start Xfer
		while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Tx) != HAL_DMA_STATE_READY);					//Wait for DMA to complete
	} else {	//neither are null, so do full tx/rx
		errorcode = HAL_SPI_TransmitReceive_DMA(SpiInfo->pHspi, TxBuffer, RxBuffer, Size);		//Start Xfer
		while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Rx) != HAL_DMA_STATE_READY);					//Wait for DMA to complete
	}
	while (SpiInfo->pHspi->State != HAL_SPI_STATE_READY);					//Wait for SPI to complete
	SpiInfo->Ncs_Port->BSRR = SpiInfo->Ncs_Pin;												//Set CS High

	return errorcode;

}

HAL_StatusTypeDef App_Spi_Tx_Rx_DMA_Block_CSnCtl(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer, uint8_t *RxBuffer, uint16_t Size, uint8_t SetCns) {
	HAL_StatusTypeDef errorcode;

	SpiInfo->Ncs_Port->BRR = SpiInfo->Ncs_Pin;												//Set CS low
	//HAL_SPIEx_FlushRxFifo(SpiInfo->pHspi);				//Flush Rx buffer
	if (TxBuffer == NULL) {		//tx buffer is null, so only receive data
		errorcode = HAL_SPI_Receive_DMA(SpiInfo->pHspi, RxBuffer, Size);		//Start Xfer
		while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Rx) != HAL_DMA_STATE_READY);					//Wait for DMA to complete
	} else if (RxBuffer == NULL) {	//rx buffer is null, so only transmit data
		errorcode = HAL_SPI_Transmit_DMA(SpiInfo->pHspi, TxBuffer, Size);		//Start Xfer
		while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Tx) != HAL_DMA_STATE_READY);					//Wait for DMA to complete
	} else {	//neither are null, so do full tx/rx
		errorcode = HAL_SPI_TransmitReceive_DMA(SpiInfo->pHspi, TxBuffer, RxBuffer, Size);		//Start Xfer
		while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Rx) != HAL_DMA_STATE_READY);					//Wait for DMA to complete
	}
	while (SpiInfo->pHspi->State != HAL_SPI_STATE_READY);					//Wait for SPI to complete
	if (SetCns == SPI_CSN_ALLOW_HIGH) {
		SpiInfo->Ncs_Port->BSRR = SpiInfo->Ncs_Pin;												//Set CS High
	}

	return errorcode;
}

HAL_StatusTypeDef App_Spi_Tx_Rx_Separate_DMA_Block(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer, uint16_t TxSize, uint8_t *RxBuffer, uint16_t RxSize) {
	HAL_StatusTypeDef errorcode;

	SpiInfo->Ncs_Port->BRR = SpiInfo->Ncs_Pin;								//Set CS low
	//HAL_SPIEx_FlushRxFifo(SpiInfo->pHspi);									//Flush Rx buffer
	errorcode = HAL_SPI_Transmit_DMA(SpiInfo->pHspi, TxBuffer, TxSize);		//Start Xfer
	while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Tx) != HAL_DMA_STATE_READY);	//Wait for DMA to complete
	errorcode |= HAL_SPI_Receive_DMA(SpiInfo->pHspi, RxBuffer, RxSize);		//Start Xfer.  Use |= in case of error in earlier call
	while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Rx) != HAL_DMA_STATE_READY);	//Wait for DMA to complete
	while (SpiInfo->pHspi->State != HAL_SPI_STATE_READY);					//Wait for SPI to complete
	SpiInfo->Ncs_Port->BSRR = SpiInfo->Ncs_Pin;								//Set CS High


	return errorcode;
}

HAL_StatusTypeDef App_Spi_Tx_Tx_Separate_DMA_Block(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer1, uint16_t TxSize1, uint8_t *TxBuffer2, uint16_t TxSize2) {
	HAL_StatusTypeDef errorcode;

	SpiInfo->Ncs_Port->BRR = SpiInfo->Ncs_Pin;								//Set CS low
	//HAL_SPIEx_FlushRxFifo(SpiInfo->pHspi);									//Flush Rx buffer
	errorcode = HAL_SPI_Transmit_DMA(SpiInfo->pHspi, TxBuffer1, TxSize1);	//Start Xfer
	while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Tx) != HAL_DMA_STATE_READY);	//Wait for DMA to complete
	errorcode |= HAL_SPI_Transmit_DMA(SpiInfo->pHspi, TxBuffer2, TxSize2);	//Start Xfer.  Use |= in case of error in earlier call
	while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Rx) != HAL_DMA_STATE_READY);	//Wait for DMA to complete
	while (SpiInfo->pHspi->State != HAL_SPI_STATE_READY);					//Wait for SPI to complete
	SpiInfo->Ncs_Port->BSRR = SpiInfo->Ncs_Pin;								//Set CS High


	return errorcode;
}

HAL_StatusTypeDef App_Spi_3Tx_Separate_DMA_Block(SpiInfoStruct *SpiInfo, uint8_t *TxBuffer1, uint16_t TxSize1, uint8_t *TxBuffer2, uint16_t TxSize2, uint8_t *TxBuffer3, uint16_t TxSize3) {
	HAL_StatusTypeDef errorcode;

	SpiInfo->Ncs_Port->BRR = SpiInfo->Ncs_Pin;								//Set CS low
	//HAL_SPIEx_FlushRxFifo(SpiInfo->pHspi);									//Flush Rx buffer
	errorcode = HAL_SPI_Transmit_DMA(SpiInfo->pHspi, TxBuffer1, TxSize1);	//Start Xfer
	while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Tx) != HAL_DMA_STATE_READY);	//Wait for DMA to complete
	errorcode |= HAL_SPI_Transmit_DMA(SpiInfo->pHspi, TxBuffer2, TxSize2);	//Start Xfer.  Use |= in case of error in earlier call
	while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Rx) != HAL_DMA_STATE_READY);	//Wait for DMA to complete
	while (SpiInfo->pHspi->State != HAL_SPI_STATE_READY);					//Wait for SPI to complete
	errorcode |= HAL_SPI_Transmit_DMA(SpiInfo->pHspi, TxBuffer3, TxSize3);	//Start Xfer.  Use |= in case of error in earlier call
	while (HAL_DMA_GetState(SpiInfo->pHdma_Spi_Rx) != HAL_DMA_STATE_READY);	//Wait for DMA to complete
	while (SpiInfo->pHspi->State != HAL_SPI_STATE_READY);					//Wait for SPI to complete
	SpiInfo->Ncs_Port->BSRR = SpiInfo->Ncs_Pin;								//Set CS High


	return errorcode;
}
