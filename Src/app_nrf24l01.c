/*
 * app_nrf24l01.c
 *
 *  Created on: May 3, 2017
 */

#include "../Inc/app_nrf24l01.h"
#include "stm32f1xx_hal.h"
#include "string.h"				//for memset


//Globals
	uint8_t Padding[32];

//Boots up the chip, and puts it in Rx Mode
void App_Nrf24L01_Init(Nrf24L01_Struct *Nrf24L01) {
	SpiInfoStruct *spi = &(Nrf24L01->Spi_Info);
	GPIO_InitTypeDef GPIO_InitStruct;
	uint8_t bufferTx[2];

	//Setup GPIO
		//Enable GPIO clocks
	if ( (Nrf24L01->CE_Port == GPIOA) || (spi->Ncs_Port == GPIOA) ) {
		__HAL_RCC_GPIOA_CLK_ENABLE();
	}
	if ( (Nrf24L01->CE_Port == GPIOB) || (spi->Ncs_Port == GPIOB) ) {
			__HAL_RCC_GPIOB_CLK_ENABLE();
	}
	if ( (Nrf24L01->CE_Port == GPIOC) || (spi->Ncs_Port == GPIOC) ) {
			__HAL_RCC_GPIOC_CLK_ENABLE();
	}
	if ( (Nrf24L01->CE_Port == GPIOD) || (spi->Ncs_Port == GPIOD) ) {
			__HAL_RCC_GPIOD_CLK_ENABLE();
	}
	if ( (Nrf24L01->CE_Port == GPIOE) || (spi->Ncs_Port == GPIOE) ) {
			__HAL_RCC_GPIOE_CLK_ENABLE();
	}

		//Config Chip Enable pin output
	GPIO_InitStruct.Pin = Nrf24L01->CE_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(Nrf24L01->CE_Port, &GPIO_InitStruct);

		//Config Chip Select pin output
	GPIO_InitStruct.Pin = spi->Ncs_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(spi->Ncs_Port, &GPIO_InitStruct);

	HAL_GPIO_WritePin(Nrf24L01->CE_Port, Nrf24L01->CE_Pin, RESET);						//Set CE pin low
	HAL_GPIO_WritePin(Nrf24L01->Spi_Info.Ncs_Port, Nrf24L01->Spi_Info.Ncs_Pin, SET);	//Set Chip Select pin high

	memset(Padding, 0, 32);	//set padding to 0

	HAL_Delay(100);		//100 ms power ramp up time
	HAL_Delay(2);		//wait 2 ms for the radio to boot. (1.5ms Tpd2stby)


	// Auto Acknowledge - All Enbled by Default
	// Set Address Width (0x03)- 1 = 3 bytes, 2 = 4 bytes, 3 = 5 bytes
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_SETUP_AW, 3);

	// Set auto retransmit (0x04) 7:4 Wait (0 =250us, 1 = 500us... 15).  3:0 Retransmit count (0 to 15)
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_SETUP_RETR, (15 << 4) | (15) );

	// Set RF Channel, 2 is default
	//Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_RF_CH, 2);		//default channel
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_RF_CH, 76);		//test channel

	// Set RF Rate and power output (0x06).
	// Plus:
	//7:6,4 set to 0.  Set bit 5 and clear bit 3 for 250 kbps.
	//Clear bit 5 and set (2 Mbps) or clear (1 Mbps) bit 3.
	//2:1 power. 00 = lowest, 11 = highest
	//														  CRLPHPPO
	//Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_RF_SETUP, 0b00000010);
	//Non plus:
	//7:5 reserved to 000
	//4: Force PLL Lock
	//3: Air Rate. 0:1Mbps, 1:2Mbps
	//2:1 power 00:lowest 11:highest
	//0: LNA Gain
	//														  000LRPPG
	//Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_RF_SETUP, 0b00000011);	//low/mid power
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_RF_SETUP, 0b00000111);	//high power

	//Unlock Features
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_UNLOCK, 0x73);	//magic val in product spec pg 46

	//Feature (0x1D).  Disable: dynamic payload, payload with ack, tx payload noack cmd
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_FEATURE, 0x00);

	Prv_Nrf24L01_Clear_Int_Flags(spi);
	Prv_Nrf24L01_Flush_Tx_Fifo(spi);
	Prv_Nrf24L01_Flush_Rx_Fifo(spi);

	//Set Pipe Tx (0x10), Write Address
	bufferTx[0] = NRF24L01_CMD_REG_WRITE | NRF24L01_REG_TX_ADDR;
	App_Spi_Tx_Tx_Separate_DMA_Block(spi, bufferTx, 1, Nrf24L01->Adr_Write, 5);

	//Set Pipe 0 Rx (0x0A), Write Address (for auto ack).
	bufferTx[0] = NRF24L01_CMD_REG_WRITE | NRF24L01_REG_RX_ADDR_P0;
	App_Spi_Tx_Tx_Separate_DMA_Block(spi, bufferTx, 1, Nrf24L01->Adr_Write, 5);

	//Set Pipe 0 rx size (0x11) to 32 bytes
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_RX_PW_P0, 32);

	//Set Pipe 1 Rx (0x0B), Read Address
	bufferTx[0] = NRF24L01_CMD_REG_WRITE | NRF24L01_REG_RX_ADDR_P1;
	App_Spi_Tx_Tx_Separate_DMA_Block(spi, bufferTx, 1, Nrf24L01->Adr_Read, 5);

	//Set Pipe 1 rx size (0x12) to 32 bytes
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_RX_PW_P1, 32);


	//Put chip in Rx Mode (Sets config register)
	Prv_Nrf24L01_Set_Mode_Rx(Nrf24L01);

}

// Returns 0 if successful, 1 if timeout
//Multipacket header: 0x55, 0x4b, ByteCount, 8-bit sum of data packets

uint8_t App_Nrf24L01_Write_Multi_Packet(Nrf24L01_Struct *Nrf24L01, uint8_t *Bfr, uint8_t Len, uint8_t ResumeInRxMode) {

	uint8_t result = 0;
	SpiInfoStruct *spi = &(Nrf24L01->Spi_Info);
	uint8_t cmdByte;
	uint8_t header[4];
	uint8_t chkSum = 0;
	uint8_t bytesToSend;

	uint8_t index;

	//calc checksum
	for (index = 0; index < Len; index++) {
		chkSum += Bfr[index];
	}

	//Setup header
	cmdByte = NRF24L01_CMD_PAYLOAD_TX_WRITE;
	header[0] = 0x55;
	header[1] = 0x4B;
	header[2] = Len;
	header[3] = chkSum;

	uint8_t sendHeader = 1;
	uint8_t packetSpaceRemaining;
	uint8_t bfrBytesRemaining = Len;

	if(Nrf24L01->Cur_Run_Mode != NRF24L01_RUN_MODE_TX) {
		Prv_Nrf24L01_Set_Mode_Tx(Nrf24L01);
	}
	else {
		//Flush Rx Buffer
		Prv_Nrf24L01_Flush_Rx_Fifo(spi);

		//Flush Tx Buffer
		Prv_Nrf24L01_Flush_Tx_Fifo(spi);

		//Clear Int flags
		Prv_Nrf24L01_Clear_Int_Flags(spi);
	}


	while ((bfrBytesRemaining > 0)  && (result==0)) {		//run Loop until all bytes in buffer are xfered, or result!=0, signifying a failed packet xfer
		packetSpaceRemaining = 32;			//RF packet size is 32 bytes

		//Send fifo write command
		App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, &cmdByte, NULL, 1, SPI_CSN_KEEP_LOW);


		if (sendHeader == 1) {		//if the header needs to get sent out
			App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, header, NULL, 4, SPI_CSN_KEEP_LOW);
			packetSpaceRemaining -= 4;		//header has 4 bytes
			sendHeader = 0;	//don't need to send header anymore
		}

		//if there are more bytes waiting to be written than there is packet size remaining, only write
		// the amount of bytes allowed by the packet size.
		bytesToSend = (bfrBytesRemaining >= packetSpaceRemaining ? packetSpaceRemaining : bfrBytesRemaining);
		packetSpaceRemaining -= bytesToSend;

		if (packetSpaceRemaining == 0) {		//if bfr bytes take all of the packet space, then padding isn't needed.
														// Finish SPI transaction by allowing CSn to go high
			App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, Bfr, NULL, bytesToSend, SPI_CSN_ALLOW_HIGH);
			Bfr += bytesToSend;							//increment buffer pointer by number of bytes xfered.
			bfrBytesRemaining -= bytesToSend;
		}
		else {									//brf bytes remaining are less then needed to fill a packet.
												// send padding after bfr is sent to complete packet.
			App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, Bfr, NULL, bytesToSend, SPI_CSN_KEEP_LOW);
			Bfr += bytesToSend;							//increment buffer pointer by number of bytes xfered.
			bfrBytesRemaining -= bytesToSend;

			App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, Padding, NULL, packetSpaceRemaining, SPI_CSN_ALLOW_HIGH);
		}

		HAL_GPIO_WritePin(Nrf24L01->CE_Port, Nrf24L01->CE_Pin, SET);						//Set CE pin high to start transmission
		//wait for packet xfer ack
		result = Prv_Nrf24L01_Wait_For_Tx(spi);
		if (result == 0) {
			Prv_Nrf24L01_Clear_Status_Flag(spi, NRF24L01_FLAG_STAT_TX_DS);		//If data was sent successfully, clear the flag for the next go around
		}

		HAL_GPIO_WritePin(Nrf24L01->CE_Port, Nrf24L01->CE_Pin, RESET);						//Set CE pin low.  Radio will go into low power mode after

	}	//end while data to be sent


	if (ResumeInRxMode==NRF24L01_FLAG_RESUME_RX) {
		Prv_Nrf24L01_Set_Mode_Rx(Nrf24L01);
	}

//	if (result == 0) 		printf("ack rxd\n");
//	else if (result == 1)	printf("Max RT reached\n");
//	else if (result == 2)	printf("Wait Timeout\n");

	return result;
}

// Returns 0 if successful,
// 		1 if timeout, no data, bfr too small
//		2 if header doesn't match
//		3 if CRC Failed.
uint8_t App_Nrf24L01_Read_Multi_Packet(Nrf24L01_Struct *Nrf24L01, uint8_t *Bfr, uint8_t *Len) {
	uint8_t result = 0;
	SpiInfoStruct *spi = &(Nrf24L01->Spi_Info);
	uint8_t packetsToRead = 0;
	uint8_t cmdByte = NRF24L01_CMD_PAYLOAD_RX_READ;
	uint8_t crcCalcd = 0;
	uint8_t bytesXfered = 0;
	uint8_t chipStat;
	uint8_t fifoStat;
	uint8_t header[4];
	uint32_t timeoutTick = HAL_GetTick() + 10;		//timeout of 10 ms for entire package.  Best case 96 bytes takes 3ms

	//Check for data ready
	chipStat = Prv_Nrf24L01_Get_Status(spi);	//reg 0x07

	//if data is ready, pull the first packet, then subsequent packets if necessary
	if ((chipStat & NRF24L01_FLAG_STAT_RX_DR) && (*Len >= 28)) {
		App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, &cmdByte, NULL, 1, SPI_CSN_KEEP_LOW);	//send cmd byte, wait until bfr to read to let CSn high
		App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, NULL, header, 4, SPI_CSN_KEEP_LOW);		//pull header bytes
		App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, NULL, Bfr, 28, SPI_CSN_ALLOW_HIGH);		//pull buffer bytes.  Allow CSn to go high
		Prv_Nrf24L01_Clear_Status_Flag(spi, NRF24L01_FLAG_STAT_RX_DR);

		//check header bytes
		if ((header[0] == 0x55) && (header[1] == 0x4b)) {
			bytesXfered +=28;
			packetsToRead = ((header[2] + 3) >> 5);		//first packet is 28 bytes data, so add 3 for header.
														// Adding 3 instead of 4 so to round down since first packet was already read
														// Divide by 32 (32 bytes per packet) (right shift 5 = /32)
		}	//end if header byte check out
		else {
			result = 2;
		}// end else header bytes


	}	//end if data ready and buffer large enough
	else {
		result = 1;		//no data ready, or Bfr is too small
	}	//end else

	//pull in any addl packets if needed.  stop if result value gets set, indicating failure
	while ( (packetsToRead > 0) && (result == 0)) {

		//check for more data
		fifoStat = Prv_Nrf24L01_Read_Register(spi, NRF24L01_REG_FIFO_STATUS);
		if (!(fifoStat & NRF24L01_FLAG_FIFO_RX_EMPTY)) {
			if (*Len >= (bytesXfered + 32) ) {	//check size of bfr
				App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, &cmdByte, NULL, 1, SPI_CSN_KEEP_LOW);	//send cmd byte, wait until bfr to read to let CSn high
				App_Spi_Tx_Rx_DMA_Block_CSnCtl(spi, NULL, &(Bfr[bytesXfered]), 32, SPI_CSN_ALLOW_HIGH);		//pull buffer bytes.  Allow CSn to go high
				Prv_Nrf24L01_Clear_Status_Flag(spi, NRF24L01_FLAG_STAT_RX_DR);

				bytesXfered += 32;
				packetsToRead--;


			}	//end if buffer large enough
			else {
				result = 1;
			}	//end else buffer not large enough
		}//end if fifo not empty

		//check for timeout
		if (HAL_GetTick() > timeoutTick) {
			result = 1;
		} //end if timeout
	}



	if (result == 0) {		//if result is still 0, then all data came in.  Check CRC (really just simeple checksum)
		for (uint8_t i = 0; i < header[2]; i++) {		//header[2] contains data size
			crcCalcd += Bfr[i];
		}

		if (crcCalcd != header[3]) {
			result = 3;
		}	//end if crc fails
		else {
			(*Len) = header[2];	//CRC passes, update Len value with number of data bytes
		}
	}

	return result;
}

// Returns 0 if successful, 1 if timeout
uint8_t App_Nrf24L01_Write_Packet(Nrf24L01_Struct *Nrf24L01, uint8_t *Bfr, uint8_t Len, uint8_t ResumeInRxMode) {
	SpiInfoStruct *spi = &(Nrf24L01->Spi_Info);

	uint8_t paddingCount = 32-Len;		//Must send 32 bytes
	uint8_t cmdByte = NRF24L01_CMD_PAYLOAD_TX_WRITE;

	uint8_t result = 0;
	uint8_t status;
	uint8_t keepWaiting;


	//Send data out
	Prv_Nrf24L01_Set_Mode_Tx(Nrf24L01);		//put radio in tx mode

	App_Spi_3Tx_Separate_DMA_Block(&(Nrf24L01->Spi_Info),
			&cmdByte, 1, Bfr, Len, Padding, paddingCount );		//Send write command, buffer, and any padding

	HAL_GPIO_WritePin(Nrf24L01->CE_Port, Nrf24L01->CE_Pin, SET);						//Set CE pin high to start transmission

	//Wait for tx success or timeout
	keepWaiting = 1;
	while (keepWaiting) {
		status = Prv_Nrf24L01_Get_Status(spi);
		if (status & NRF24L01_FLAG_STAT_TX_DS) {	//check for Data Sent flag
			keepWaiting = 0;	//stop the loop
		}
		else if (status & NRF24L01_FLAG_STAT_MAX_RT) {	//check for max retries reached
			keepWaiting = 0;	//stop the loop
			result = 1;		//set result as timeout
			Prv_Nrf24L01_Clear_Status_Flag(spi, NRF24L01_FLAG_STAT_MAX_RT);

		}

	}

	HAL_GPIO_WritePin(Nrf24L01->CE_Port, Nrf24L01->CE_Pin, RESET);						//Set CE pin low.  Radio will go into low power mode after
																						// Tx completes.

	if (ResumeInRxMode) {
		//Complete, put radio back in rx mode
		Prv_Nrf24L01_Set_Mode_Rx(Nrf24L01);		//put radio in tx mode
	}

	return result;
}


//Returns 0 if successful, 1 for timeout / no data rxd
// Brf must be large enough to contain all the data (Multiple packets are possible)
// Len gets modified with actual number of bytes read
uint8_t App_Nrf24L01_Read_Packets(Nrf24L01_Struct *Nrf24L01, uint8_t *Bfr, uint8_t *Len) {
	SpiInfoStruct *spi = &(Nrf24L01->Spi_Info);
	uint8_t result = 1;	//default to no data
	uint8_t fifoStat;
	uint8_t chipStat;
	uint8_t bytesXfered = 0;
	uint8_t cmd = NRF24L01_CMD_PAYLOAD_RX_READ;
	uint8_t readMore;

	chipStat = Prv_Nrf24L01_Get_Status(spi);	//reg 0x07
	fifoStat = Prv_Nrf24L01_Read_Register(spi, NRF24L01_REG_FIFO_STATUS);

	readMore = chipStat & NRF24L01_FLAG_STAT_RX_DR;

	//Pull data while there is data in the RF Fifo, and the Brf has space
	while ( readMore && (bytesXfered + 32 <= (*Len) ) ) {
		App_Spi_Tx_Rx_Separate_DMA_Block(spi, &cmd, 1, Bfr, 32);
		Prv_Nrf24L01_Clear_Status_Flag(spi, NRF24L01_FLAG_STAT_RX_DR);
		bytesXfered += 32;
		fifoStat = Prv_Nrf24L01_Read_Register(spi, NRF24L01_REG_FIFO_STATUS);
		readMore = (!(fifoStat & NRF24L01_FLAG_FIFO_RX_EMPTY));
		result = 0;		//set flag that data was rxd
	}

	(*Len) = bytesXfered;

	return result;
}

///   ***   Private Functions   ***

// Read from Pipe 1.  Pipe 0 is used for auto ack, so leave that one alone
void Prv_Nrf24L01_Set_Mode_Rx(Nrf24L01_Struct *Nrf24L01) {

	SpiInfoStruct *spi = &(Nrf24L01->Spi_Info);

	HAL_GPIO_WritePin(Nrf24L01->CE_Port, Nrf24L01->CE_Pin, RESET);						//Set CE pin low
	HAL_GPIO_WritePin(Nrf24L01->Spi_Info.Ncs_Port, Nrf24L01->Spi_Info.Ncs_Pin, SET);	//Set Chip Select pin high


	//Flush Rx Buffer
	Prv_Nrf24L01_Flush_Rx_Fifo(spi);

	//Clear int flags
	Prv_Nrf24L01_Clear_Int_Flags(spi);

	//Enable Rx Pipes (0x02) - Pipe 1
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_EN_RXADDR, 2);		//1 = pipe 0, 2 = pipe 1...

	//In Config register (0x00), set CRC (b3), CRC 2 bytes (b2)
	//							Power up (b1), RX mode (b0).  Keep upper bits clear.
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_CONFIG, 0x0F);

	HAL_GPIO_WritePin(Nrf24L01->CE_Port, Nrf24L01->CE_Pin, SET);						//Set CE pin high

	Nrf24L01->Cur_Run_Mode = NRF24L01_RUN_MODE_RX;

}

void Prv_Nrf24L01_Set_Mode_Tx(Nrf24L01_Struct *Nrf24L01) {
	SpiInfoStruct *spi = &(Nrf24L01->Spi_Info);

	HAL_GPIO_WritePin(Nrf24L01->CE_Port, Nrf24L01->CE_Pin, RESET);						//Set CE pin low
	HAL_GPIO_WritePin(Nrf24L01->Spi_Info.Ncs_Port, Nrf24L01->Spi_Info.Ncs_Pin, SET);	//Set Chip Select pin high


	//Enable Pipe 0
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_EN_RXADDR, 1);		//1 = pipe 0, 2 = pipe 1...


	//Flush Rx Buffer
	Prv_Nrf24L01_Flush_Rx_Fifo(spi);

	//Flush Tx Buffer
	Prv_Nrf24L01_Flush_Tx_Fifo(spi);

	//Clear Int flags
	Prv_Nrf24L01_Clear_Int_Flags(spi);

	//In Config register (0x00), set CRC (b3), CRC 2 bytes (b2)
	//							Power up (b1).  Keep RX mode (b0) and upper bits clear.
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_CONFIG, 0x0E);

	Nrf24L01->Cur_Run_Mode = NRF24L01_RUN_MODE_TX;

}

void Prv_Nrf24L01_Write_Register(SpiInfoStruct *Spi, uint8_t RegAdr, uint8_t RegData) {
	uint8_t bufferTx[2];
	bufferTx[0] = NRF24L01_CMD_REG_WRITE + RegAdr;		//Doing an add instead of bit or to help
	bufferTx[1] = RegData;								//with Enable Feature cmd
	App_Spi_Tx_Rx_DMA_Block(Spi, bufferTx, NULL, 2);

}
uint8_t Prv_Nrf24L01_Read_Register(SpiInfoStruct *Spi, uint8_t RegAdr) {
	uint8_t bufferTx[1];
	uint8_t bufferRx[1];

	bufferTx[0] = NRF24L01_CMD_REG_READ | RegAdr;
	App_Spi_Tx_Rx_Separate_DMA_Block(Spi, bufferTx, 1, bufferRx, 1);

	return bufferRx[0];
}

// Returns 0 if FIFO becomes empty, 1 if we timeout
uint8_t Prv_Nrf24L01_Wait_For_Empty_Tx(SpiInfoStruct *Spi) {

	uint32_t timeoutTick;
	uint8_t regValue;


	timeoutTick = HAL_GetTick() + 1000;		//Have timeout of 1 sec

	do {
		regValue = Prv_Nrf24L01_Read_Register(Spi, NRF24L01_REG_FIFO_STATUS);
	}
	while ( ( (regValue & NRF24L01_FLAG_FIFO_TX_EMPTY)== 0) && (HAL_GetTick() < timeoutTick)  );		//keep running until either the TX_Empty flag is set
																										//or a timeout elapses
	if ( (regValue & NRF24L01_FLAG_FIFO_TX_EMPTY)== 0) {
		return 1;				//If Fifo Tx Empty flag is still clear, then the timeout elapsed
	}

	return 0;

}

//returns 0 if xfer is successful, 1 if retry limit is reached, 2 if function timesout
uint8_t Prv_Nrf24L01_Wait_For_Tx(SpiInfoStruct *Spi) {

	uint8_t result = 0;
	uint32_t timeoutTick = HAL_GetTick() + 10;		//wait max 10ms
	uint8_t keepWaiting;
	uint8_t status;

	//Wait for tx success or timeout
	keepWaiting = 1;
	while (keepWaiting ) {
		status = Prv_Nrf24L01_Get_Status(Spi);
		if (status & NRF24L01_FLAG_STAT_TX_DS) {	//check for Data Sent flag
			keepWaiting = 0;	//stop the loop
		}
		else if (status & NRF24L01_FLAG_STAT_MAX_RT) {	//check for max retries reached
			keepWaiting = 0;	//stop the loop
			result = 1;		//set result as timeout
			Prv_Nrf24L01_Clear_Status_Flag(Spi, NRF24L01_FLAG_STAT_MAX_RT);

		}
		else if (HAL_GetTick() > timeoutTick) {
			keepWaiting = 0;	//stop the loop
			result = 2;		//set result as function timeout
		}

	}

	return result;
}

void Prv_Nrf24L01_Clear_Int_Flags(SpiInfoStruct *Spi) {
	Prv_Nrf24L01_Write_Register(Spi, NRF24L01_REG_STATUS, 0x70);	//writing 1 to int bits clears them
																	//Remaining bits are read only
}

void Prv_Nrf24L01_Flush_Tx_Fifo(SpiInfoStruct *Spi) {
	uint8_t cmd = NRF24L01_CMD_FLUSH_TX;
	App_Spi_Tx_Rx_DMA_Block(Spi, &cmd, NULL, 1);
}

void Prv_Nrf24L01_Flush_Rx_Fifo(SpiInfoStruct *Spi) {
	uint8_t cmd = NRF24L01_CMD_FLUSH_RX;
	App_Spi_Tx_Rx_DMA_Block(Spi, &cmd, NULL, 1);
}

uint8_t Prv_Nrf24L01_Get_Status(SpiInfoStruct *Spi) {
	uint8_t result;

	result = Prv_Nrf24L01_Read_Register(Spi, NRF24L01_REG_STATUS);

	return result;
}

void Prv_Nrf24L01_Clear_Status_Flag(SpiInfoStruct *Spi, uint8_t Flag) {
	Prv_Nrf24L01_Write_Register(Spi, NRF24L01_REG_STATUS, Flag);
}


///   ***   Test Functions
void Tst_Nrf24L01_Spi_RMW(Nrf24L01_Struct *Nrf24L01) {

	uint8_t origVal;
	uint8_t newVal;
	//uint8_t checkNewVal;
	//uint8_t checkOldVal;
	SpiInfoStruct *spi = &(Nrf24L01->Spi_Info);

	origVal = Prv_Nrf24L01_Read_Register(spi, NRF24L01_REG_CONFIG);			//Read register value
	newVal = origVal ^ 0x05;  //flip bits 0 and 2
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_CONFIG, newVal);			//Write new value
	//checkNewVal = Prv_Nrf24L01_Read_Register(spi, NRF24L01_REG_CONFIG);		//Confirm value was changed
	Prv_Nrf24L01_Write_Register(spi, NRF24L01_REG_CONFIG, origVal);			//write orig val back
	//checkOldVal = Prv_Nrf24L01_Read_Register(spi, NRF24L01_REG_CONFIG);		//Confirm value was reverted
	newVal += 0;


}

